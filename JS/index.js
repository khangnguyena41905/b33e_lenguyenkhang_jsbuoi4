// bài tập 1

function handle_1() {
  var num1 = +document.getElementById("so-thu-nhat").value;
  var num2 = +document.getElementById("so-thu-hai").value;
  var num3 = +document.getElementById("so-thu-ba").value;
  var result1El = document.getElementById("result1");
  //   so sánh
  if (num1 == num2 || num1 == num3 || num2 == num3) {
    result1El.innerHTML = `<h1>Nhập ba số khác nhau</h1>`;
  } else {
    if (num1 > num2 && num1 > num3) {
      if (num2 > num3) {
        result1El.innerHTML = `<h1>${num3} < ${num2} < ${num1}</h1>`;
      } else {
        result1El.innerHTML = `<h1>${num2} < ${num3} < ${num1}</h1>`;
      }
    }
    if (num2 > num1 && num2 > num3) {
      if (num1 > num3) {
        result1El.innerHTML = `<h1>${num3} < ${num1} < ${num2}</h1>`;
      } else {
        result1El.innerHTML = `<h1>${num1} < ${num3} < ${num2}</h1>`;
      }
    }
    if (num3 > num1 && num3 > num2) {
      if (num1 > num2) {
        result1El.innerHTML = `<h1>${num2} < ${num1} < ${num3}</h1>`;
      } else {
        result1El.innerHTML = `<h1>${num1} < ${num2} < ${num3}</h1>`;
      }
    }
  }
}

// bài tập 2

function handle_2() {
  var memberEl = document.getElementById("family").value;
  var result2El = document.getElementById("result2");
  switch (memberEl) {
    case "1":
      result2El.innerHTML = `<h1>Chào người lạ ơi!</h1>`;
      break;
    case "2":
      result2El.innerHTML = `<h1>Chào bố!</h1>`;
      break;
    case "3":
      result2El.innerHTML = `<h1>Chào mẹ!</h1>`;
      break;
    case "4":
      result2El.innerHTML = `<h1>Chào anh trai!</h1>`;
      break;
    case "5":
      result2El.innerHTML = `<h1>Chào em gái!</h1>`;
      break;
  }
}

/**
 * Bài tập 3
 *
 * Input: nhập 3 số bất kì
 *
 * Step:
 *      Step 1: Dùng toán tử % để kiểm tra chẵn lẻ
 *      Step 2: Dùng if và biến đếm để đếm xem có bao nhiêu số lẻ
 *      Step 3: Số chẵn = 3- biến đếm số lẻ
 *
 * Output: Bao nhiêu số chẵn, bao nhiêu số lẻ
 */

function handle_3() {
  var num1Ex3 = +document.getElementById("so-thu-nhat-ex3").value;
  var num2Ex3 = +document.getElementById("so-thu-hai-ex3").value;
  var num3Ex3 = +document.getElementById("so-thu-ba-ex3").value;
  var countEx3 = 0;

  if (num1Ex3 % 2 != 0) {
    countEx3++;
  }
  if (num2Ex3 % 2 != 0) {
    countEx3++;
  }
  if (num3Ex3 % 2 != 0) {
    countEx3++;
  }
  document.getElementById(
    "result3"
  ).innerHTML = ` <h1>Có ${countEx3} số lẻ và ${3 - countEx3} số chẵn</h1>`;
}

/**
 * Bài tập 4:
 *
 * Input: nhập 3 cạnh của tam giác
 *
 * Step : Sử dụng if else để kiểm tra
 *    -Nếu ba cạnh bằng nhau : tam giác đều
 *    -Nếu có hai cạnh bằng nhau : tam giác cân
 *    -Nếu thỏa mãn định lý pytago là tam giác vuông
 *    -Nếu khác ba loại trên thì xuất ra mãn hình " là một loại tam giác nào đó"
 *
 * Output: in kết quả
 */

function handle_4() {
  // gọi ba cạnh lần lượt là a b c
  var a = +document.getElementById("so-thu-nhat-ex4").value;
  var b = +document.getElementById("so-thu-hai-ex4").value;
  var c = +document.getElementById("so-thu-ba-ex4").value;
  var result4 = document.getElementById("result4");
  if (a == b && a == c) {
    result4.innerHTML = `<h1> Đây là tam giác đều </h1>`;
  } else if (a == b || a == c || b == c) {
    result4.innerHTML = `<h1> Đây là tam giác cân </h1>`;
  } else if (
    a * a == b * b + c * c ||
    b * b == a * a + c * c ||
    c * c == a * a + b * b
  ) {
    result4.innerHTML = `<h1> Đây là tam giác vuông </h1>`;
  } else {
    result4.innerHTML = `<h1> Đây là một loại tam giác nào đó </h1>`;
  }
}
